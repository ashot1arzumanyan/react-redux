const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 8080;

const { 
    register, 
    login, 
    checkToken, 
    resetPassword, 
    resetPasswordSendEmail,
    verifyEmail
} = require('./server/routes/apis');
const { 
    registerValidator, 
    loginValidator, 
    resetPasswordValidator,
    resetPasswordSendEmailValidator } = require('./server/validators');
// const getProposals = require('./server/routes/getProposals');
// const getDemands = require('./server/routes/getDemands');
const { getSumOf } = require('./server/routes/sumOf');
const lang = require('./server/routes/lang');
const auth = require('./server/routes/auth');
const getStatements = require('./server/routes/getStatements');

const mongoDBUrl = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@ds111913.mlab.com:11913/kanach-karmir`;

mongoose.connect(mongoDBUrl, { useNewUrlParser: true });   
mongoose.set('useFindAndModify', false);                                                   
const db = mongoose.connection;                                                                                                                                      
                                                                                                                                                                     
db.on('error', (err) => {
    console.error(err);
});                                                                                                     
db.once('open', () => {                                                                                                                                              
    console.log('mongoose connected');                                                                                                                               
});

app.disable('x-powered-by');
// app.set('etag', false);
app.use(express.static(path.join(__dirname, 'client', 'build')));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/auth', auth);

// app.use('/getProposals', getProposals);
// app.use('/getDemands', getDemands);

app.post('/register', registerValidator, register);
app.post('/login', loginValidator, login);
app.get('/checkToken', checkToken);
app.put('/resetPassword', resetPasswordValidator, resetPassword);
app.post('/resetPasswordSendEmail', resetPasswordSendEmailValidator, resetPasswordSendEmail);
app.get('/verifyEmail', verifyEmail);
app.get('/getSumOf', getSumOf);
app.get('/lang/:id', lang);
app.get('/getStatements', getStatements);

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
    console.log('remoteAddress  ' + req.connection.remoteAddress);
    console.log('remotePort  ' + req.connection.remotePort);
    console.log('localAddress  ' + req.connection.localAddress);
    console.log('localPort  ' + req.connection.localPort);
    console.log('ip  ' + req.ip);
    console.log('hostname  ' + req.hostname);
})


console.log('rss : ' + process.memoryUsage().rss / (1024 * 1024));
console.log('external : ' + process.memoryUsage().external / (1024 * 1024));
console.log('heapTotal : ' + process.memoryUsage().heapTotal / (1024 * 1024));
console.log('heapUsed : ' + process.memoryUsage().heapUsed / (1024 * 1024));
console.log('system : ' + process.cpuUsage().system / ( 1024 * 1024 ));
console.log('user : ' + process.cpuUsage().user / ( 1024 * 1024 ));

app.listen(port, () => {
    console.log('listening on ' + port);
});