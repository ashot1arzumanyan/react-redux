const SumOfRegion = require('../collections/SumOfRegion'); 
const SumOfCity = require('../collections/SumOfCity'); 
const SumOfType = require('../collections/SumOfType'); 
const SumOfSubType = require('../collections/SumOfSubType'); 

const getSumOfByModulName = (modul, name) => {
    return new Promise((resolve, reject) => {
        modul.findOneAndUpdate({}, {}, { new: true }, (err, doc) => {
            if (err) {
                console.error(err);
                reject(err)
            } else {
                resolve({ [name]: doc })
            }
        })        
    })
}

module.exports.getSumOf = (req, res) => {
    Promise.all([
        getSumOfByModulName(SumOfRegion, 'region'),
        getSumOfByModulName(SumOfCity, 'city'),
        getSumOfByModulName(SumOfType, 'type'),
        getSumOfByModulName(SumOfSubType, 'subType')
    ])
        .then(values => {
            res.setHeader('Cache-Control', 'no-cache, no-store');
            const data = Object.assign({}, values[0], values[1], values[2], values[3])
            res.end(JSON.stringify(data))
        }).catch(err => {
            console.log(err);
            res.status(500).end()
        });
};

const increment = (modul, key) => {
    return new Promise((resolve, reject) => {
        modul.findOneAndUpdate(
            {}, 
            { 
                $inc: { [key]: 1 }
            },
            {
                setDefaultsOnInsert: true
            },
            (err) => {
                if(err) {
                    reject(err)
                } else {
                    resolve()
                    console.log(key + ' incremented by 1');
                }
            }
        )
    }) 
} 

const decrement = (modul, key) => {
    return new Promise((resolve, reject) => {
        modul.findOneAndUpdate(
            {}, 
            { 
                $inc: { [key]: -1 }
            },
            {
                setDefaultsOnInsert: true
            },
            (err) => {
                if(err) {
                    reject(err)
                } else {
                    resolve()
                    console.log(key + ' decremented by 1');
                }
            }
        )
    }) 
} 

module.exports.incrementSumOfAll = (proposalOrDemand, region, city, type, subType) => {
    const keyOfRegion = region + '.' + proposalOrDemand;
    const keyOfcity = city + '.' + proposalOrDemand;
    const keyOfType = type + '.' + proposalOrDemand;
    const keyOfSubType = subType + '.' + proposalOrDemand;
    
    return Promise.all([
        increment(SumOfRegion, keyOfRegion),
        increment(SumOfCity, keyOfcity),
        increment(SumOfType, keyOfType),
        increment(SumOfSubType, keyOfSubType)
    ])
}

module.exports.decrementSumOfAll = (proposalOrDemand, region, city, type, subType) => {
    const keyOfRegion = region + '.' + proposalOrDemand;
    const keyOfcity = city + '.' + proposalOrDemand;
    const keyOfType = type + '.' + proposalOrDemand;
    const keyOfSubType = subType + '.' + proposalOrDemand;

    return Promise.all([
        decrement(SumOfRegion, keyOfRegion),
        decrement(SumOfCity, keyOfcity),
        decrement(SumOfType, keyOfType),
        decrement(SumOfSubType, keyOfSubType)
    ])
}