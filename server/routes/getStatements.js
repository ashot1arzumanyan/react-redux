const myCache = require('../helpers/myCache');
const Demand = require('../collections/Demand');
const Proposal = require('../collections/Proposal');

let p_link_to_delete = '';
let d_link_to_delete = '';
let p_skip = 0;
let p_link = '';
let d_skip = 0;
let d_link = '';

const generateCacheForDemand = (skip, linkToCachedAll) => {
    if (linkToCachedAll !== '') {
        Demand.find({}).sort({date: -1}).skip(skip).exec((err, demands) => {
            if (err) {
                console.error(err);
            } else {
                myCache.set(linkToCachedAll, demands, 600, (err) => {
                    if (err) {
                        console.log(err);
                    }
                })
            }
        })
        d_link = '';
    }
}

const generateCacheForProposal = (skip, linkToCachedAll) => {
    if (linkToCachedAll !== '') {
        Proposal.find({}).sort({date: -1}).skip(skip).exec((err, proposals) => {
            if (err) {
                console.error(err);
            } else {
                myCache.set(linkToCachedAll, proposals, 600, (err) => {
                    if (err) {
                        console.log(err);
                    }
                })
            }
        })
        p_link = ''
    }
}

const deleteCacheForProposal = (linkToCachedAll) => {
    if (linkToCachedAll !== '') {
        myCache.del(linkToCachedAll, (err) => {
            if (err) {
                console.log(err);
            }
        })
    }
    p_link_to_delete = '';
}

const deleteCacheForDemand = (linkToCachedAll) => {
    if (linkToCachedAll !== '') {
        myCache.del(linkToCachedAll, (err) => {
            if (err) {
                console.log(err);
            }
        })
    }
    d_link_to_delete = '';
}

const getDemands = (skip, link) => {
    return new Promise((resolve, reject) => {
        if (skip === 0) {
            Demand.estimatedDocumentCount((err, count) => {
                if (err) {
                    console.log(err);
                    reject(err)
                } else {
                    Demand.find({}).sort({date: -1}).skip(skip).limit(3).exec((err, demands) => {
                        if (err) {
                            console.error(err);
                            reject(err)
                        } else {
                            const generatedLink = `d${Date.now()}`;
                            d_skip = skip + 3;
                            d_link = generatedLink;
                            resolve({
                                demands: {
                                    demands: demands,
                                    skip: skip + 3,
                                    count: count,
                                    linkToCachedAll: generatedLink
                                }
                            })
                            // setTimeout(() => {
                            //     generateCacheForDemand(skip + 3, generatedLink)
                            // }, 1000);
                        }
                    })
                }
            })
        } else {
            myCache.get(link, (err, data) => {
                if (err || !data ) {
                    console.log(err);
                    Demand.find({}).sort({date: -1}).skip(skip).limit(3).exec((err, demands) => {
                        if (err) {
                            console.error(err);
                            reject(err)
                        } else {
                            const generatedLink = `d${Date.now()}`;
                            d_link_to_delete = link;
                            d_skip = skip + 3;
                            d_link = generatedLink;
                            resolve({
                                demands: {
                                    demands: demands,
                                    skip: skip + 3,
                                    linkToCachedAll: generatedLink
                                }
                            })
                            // setImmediate(deleteCache, link)
                            // setImmediate(generateCacheForDemand, skip + 3, generatedLink);
                        }
                    })
                } else {
                    d_link_to_delete = link;
                    resolve({
                        demands: {
                            demands: data,
                        }
                    })
                    // setImmediate(deleteCache, link)
                }
            })
    
        }
    })
}

const getProposals = (skip, link) => {
    return new Promise((resolve, reject) => {
        if (skip === 0) {
            Proposal.estimatedDocumentCount((err, count) => {
                if (err) {
                    console.log(err);
                    reject(err)
                } else {
                    Proposal.find().sort({ date: -1 }).skip(skip).limit(3).exec((err, proposals) => {
                        if (err) {
                            console.error(err);
                            reject(err)
                        } else {
                            const generatedLink = `p${Date.now()}`;
                            p_skip = skip + 3;
                            p_link = generatedLink;
                            resolve({
                                proposals: {
                                    proposals: proposals,
                                    skip: skip + 3,
                                    count: count,
                                    linkToCachedAll: generatedLink   
                                }
                            })
                            // setImmediate(generateCacheForProposal, skip + 3, generatedLink);
                            // setTimeout(() => {
                            //     generateCacheForProposal(skip + 3, generatedLink)
                            // }, 0);
                        }
                    })       
                }
            })
        } else {
            myCache.get(link, (err, data) => {
                if (err || !data) {
                    console.log(err);
                    Proposal.find({}).sort({date: -1}).skip(skip).limit(3).exec((err, proposals) => {
                        if (err) {
                            console.error(err);
                            reject(err)
                        } else {
                            const generatedLink = `p${Date.now()}`;
                            p_link_to_delete = link;
                            p_skip = skip + 3;
                            p_link = generatedLink;
                            resolve({
                                proposals: {
                                    proposals: proposals,
                                    skip: skip + 3,
                                    linkToCachedAll: generatedLink   
                                }
                            })
                            // setImmediate(deleteCache, link)
                            // setImmediate(generateCacheForProposal, skip + 3, generatedLink);
                        }
                    })
                } else {
                    p_link_to_delete = link;
                    resolve({
                        proposals: {
                            proposals: data
                        }
                    })
                    // setImmediate(deleteCache, link)
                }
            })
        }
    })
}

module.exports = (req, res) => {
    Promise.all([
        getProposals(Number(req.query.p_skip), req.query.p_link),
        getDemands(Number(req.query.d_skip), req.query.d_link)
    ])
    .then(data => {
        res.send({ ...data[0], ...data[1] });
        generateCacheForProposal(p_skip, p_link);
        generateCacheForDemand(d_skip, d_link);
        deleteCacheForProposal(p_link_to_delete);
        deleteCacheForDemand(d_link_to_delete);
    })
    .catch(err => {
        res.status(500).end()
        console.log(err);
    }) 
}