const express = require('express');
const myCache = require('../helpers/myCache');

const router = express.Router();

const Demand = require('../collections/Demand');

router.get('/:linkToCachedAll', (req, res) => {
    const skip = Number(req.query.skip)
    console.log(skip);
    console.log(myCache.getStats())
    if (skip === 0) {
        Demand.estimatedDocumentCount((err, count) => {
            if (err) {
                console.log(err);
                return res.send({ok: false})
            } else {
                Demand.find({}).sort({date: -1}).skip(skip).limit(3).exec((err, demands) => {
                    if (err) {
                        console.error(err);
                        return res.json({ok: false})
                    } else {
                        const generateLink = Date.now();
                        setImmediate(generateCache, skip + 3, generateLink);
                        return res.json({ok: true, demands: demands, skip: skip + 3, count: count, linkToCachedAll: generateLink})
                    }
                })
            }
        })
    } else {
        const linkToCachedAll = req.params.linkToCachedAll;
        myCache.get(linkToCachedAll, (err, data) => {
            if (err || !data ) {
                console.log(err);
                Demand.find({}).sort({date: -1}).skip(skip).limit(3).exec((err, demands) => {
                    if (err) {
                        console.error(err);
                        return res.json({ok: false})
                    } else {
                        const generateLink = Date.now();
                        setImmediate(deleteCache, linkToCachedAll)
                        setImmediate(generateCache, skip + 3, generateLink);
                        return res.json({ok: true, demands: demands, skip: skip + 3, linkToCachedAll: generateLink})
                    }
                })
            } else {
                setImmediate(deleteCache, linkToCachedAll)
                res.json({ok: true, demands: data})
            }
        })

    }
})

function generateCache(skip, linkToCachedAll) {
    console.log(skip, linkToCachedAll);
    Demand.find({}).sort({date: -1}).skip(skip).exec((err, demands) => {
        if (err) {
            console.error(err);
        } else {
            myCache.set(linkToCachedAll, demands, 600, (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log(data);
                }
            })
        }
    })
}

function deleteCache(linkToCachedAll) {
    myCache.del(linkToCachedAll, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
        }
    })
}

module.exports = router