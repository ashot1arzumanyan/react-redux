const express = require('express');
const { validationResult } = require('express-validator/check');
const bcrypt = require('bcryptjs');
const JWT = require('jsonwebtoken');

const router = express.Router();

const User = require('../collections/User');
const Demand = require('../collections/Demand');
const Proposal = require('../collections/Proposal');

const { incrementSumOfAll, decrementSumOfAll } = require('./sumOf')

const { 
    updatePassvordValidator, 
    demandValidator, 
    proposalValidator,
    changePersonalInfoValidator } = require('../validators');
const checkAuth = require('../helpers/checkAuth');
const generateUserData = require('../helpers/generateUserData'); 

const getDemandsById = (req, res) => {
    const _id = req.user._id;
    Demand.find({user_id: _id}).sort({date: -1}).exec((err, demands) => {
        if (err) {
            console.error(err);
            res.status(500).end()
        }
        res.send(demands)
    })
}

const getProposalsById = (req, res) => {
    const _id = req.user._id;
    Proposal.find({user_id: _id}).sort({date: -1}).exec((err, proposals) => {
        if (err) {
            console.error(err);
            res.status(500).end()
        } else {
            res.send(proposals)
        }
    })
}

const changePersonalInfo = (req, res) => {
    const data = req.body
    User.findByIdAndUpdate(req.user._id, data, {new: true}, (err, user) => {
        if (err || !user) {
            console.log(err || 'user not found');
            res.status(500).end()
        } else {
            const data = generateUserData(user);
            JWT.sign(
                data,
                process.env.MY_SECRET_KEY, 
                { expiresIn: '1h' }, 
                (err, token) => {
                    if(err) {
                        console.error(err);
                        res.status(500).end()
                    } else {
                        res.json({ 
                            access_token: token, 
                            user: data
                        })
                    }
                })
        }
    })
}

const updatePassword = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array())
        res.status(400).send({ errors: errors.array() })
    } else {
        const { old_password, password } = req.body
        User.findById(req.user._id, (err, doc) => {
            if (err || !doc) {
                console.log(err || 'user not found');
                res.status(500).end()
            } else {
                bcrypt.compare(old_password, doc.password, (err, isMatch) => {
                    if (err || !isMatch) {
                        console.log(err || 'wrong old password');
                        res.status(400).send({errors: ['wrong_current_password']})
                    } else {
                        bcrypt.genSalt(10, (err, salt) => {
                            if (err) {
                                console.log(err);
                                return res.status(500).end()
                            }
                            bcrypt.hash(password, salt, (err, hash) => {
                                if (err) {
                                    console.log(err);
                                    return res.status(500).end()
                                }
                                User.findByIdAndUpdate(req.user._id, {password: hash}, (err) => {
                                    if (err) {
                                        console.log(err);
                                        return res.status(500).end()
                                    } else {
                                        res.end()
                                    }
                                })
                            })
                        })
                    }
                })
            }
        })
    }
}

const demandStatementAdd = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).end()
    } else {       
        const body = req.body; 
        const authorizationHeader = req.header('Authorization');
        if (authorizationHeader && authorizationHeader.split(' ').length) {
            const access_token = req.header('Authorization').split(' ')[1];
            JWT.verify(access_token, process.env.MY_SECRET_KEY, (err, decoded) => {
                if(err || !decoded) {
                    console.error(err || 'not auth');
                    res.status(403).end()
                } else {
                    const demand = new Demand({
                        region: body.region,
                        city: body.city,
                        type: body.type,
                        subType: body.subType,
                        unit: body.unit,
                        price: body.price,
                        quantity: body.quantity,
                        oneTime: body.oneTime,
                        continuous: body.continuous,
                        is_mark_the_date: body.is_mark_the_date,
                        description_one_word: body.description_one_word,
                        comment: body.comment,
                        username: body.username,
                        phone: body.phone,
                        email: body.email,
                        user_id: decoded._id
                    });
                    if (body.is_mark_the_date) {
                        demand.year = body.year
                        demand.month = body.month
                        if (body.day !== '') {
                            demand.day = body.day
                        }
                    }
                    if (body.continuous) {
                        demand.continuousType = body.continuousType
                        demand.frequencyNum = body.frequencyNum
                    }
                    demand.save((err, doc) => {
                        if (err) {
                            console.error(err);
                            res.status(500).end()
                        } else {
                            res.send(doc);
                            setImmediate(incrementSumOfAll, 'demand', body.region, body.city, body.type, body.subType);
                        }
                    })
                }
            })
        }
    }
}

const demandStatementDelete = (req, res) => {
    const _id = req.body._id;
    Demand.findByIdAndRemove(_id, (err, doc) => {
        if(err || !doc) {
            console.log(err);
            res.status(500).end()
        } else {
            res.end();
            setImmediate(decrementSumOfAll, 'demand', doc.region, doc.city, doc.type, doc.subType);
        }
    })
}

const demandStatementEdit = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).end()
    } else {    
        const body = req.body
        Demand.findOne({}, (err, demand) => {
            if (err || !demand) {
                console.log(err || 'doc not found');
                res.status(500).end()                
            } else {
                process.nextTick(
                    decrementSumOfAll,
                    'demand',
                    demand.region,
                    demand.city,
                    demand.type,
                    demand.subType
                );

                demand.region = body.region;
                demand.city = body.city;
                demand.type = body.type;
                demand.subType = body.subType;
                demand.unit = body.unit;
                demand.price = body.price;
                demand.quantity = body.quantity;
                demand.oneTime = body.oneTime;
                demand.continuous = body.continuous;
                demand.is_mark_the_date = body.is_mark_the_date;
                demand.description_one_word = body.description_one_word;
                demand.comment = body.comment;
                demand.username = body.username;
                demand.phone = body.phone;
                demand.email = body.email;
                demand.user_id = body.user_id;
                if (body.is_mark_the_date) {
                    demand.year = body.year
                    demand.month = body.month
                    if (body.day !== '') {
                        demand.day = body.day
                    }
                }
                if (body.continuous) {
                    demand.continuousType = body.continuousType
                    demand.frequencyNum = body.frequencyNum
                }
                demand.save((err, doc) => {
                    if (err || !doc) {
                        console.log(err || 'doc not found');
                        res.status(500).end()
                    } else {
                        res.send(doc);

                        setImmediate(
                            incrementSumOfAll,
                            'demand',
                            doc.region,
                            doc.city,
                            doc.type,
                            doc.subType
                        );
                    }                    
                })
            }
        })
    }
}

const proposalStatementAdd = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).end()
    } else {
        const body = req.body;
        const authorizationHeader = req.header('Authorization');
        if (authorizationHeader && authorizationHeader.split(' ').length) {
            const access_token = req.header('Authorization').split(' ')[1];
            JWT.verify(access_token, process.env.MY_SECRET_KEY, (err, decoded) => {
                if(err || !decoded) {
                    console.error(err || 'not auth');
                    res.status(403).end()
                } else {
                    const proposal = new Proposal({
                        region: body.region,
                        city: body.city,
                        type: body.type,
                        subType: body.subType,
                        unit: body.unit,
                        price: body.price,
                        available_quantity: body.available_quantity,
                        is_plan_to_have: body.is_plan_to_have,
                        oneTime: body.oneTime,
                        continuous: body.continuous,
                        description_one_word: body.description_one_word,
                        comment: body.comment, 
                        username: body.username,
                        email: body.email,
                        phone: body.phone,
                        user_id: decoded._id
                    });
                    if (body.is_plan_to_have) {
                        proposal.plan_to_have_quantity = body.plan_to_have_quantity
                        proposal.plan_to_have_price = body.plan_to_have_price
                        if (body.continuous) {
                            proposal.continuousType = body.continuousType
                            proposal.frequencyNum = body.frequencyNum
                        } else {
                            proposal.year = body.year
                            proposal.month = body.month
                            if (body.day !== '') {
                                proposal.day = body.day
                            }
                        }
                    }
                    proposal.save((err, doc) => {
                        if (err) {
                            console.error(err);
                            res.status(500).end()
                        }
                        res.send(doc);
                        setImmediate(incrementSumOfAll, 'proposal', body.region, body.city, body.type, body.subType);
                    })
    
                }
            })
        }
    }
}

const proposalStatementDelete = (req, res) => {
    const _id = req.body._id;
    Proposal.findByIdAndRemove(_id, (err, doc) => {
        console.log(doc);
        if(err || !doc) {
            console.log(err);
            res.status(500).end()
        } else {
            res.end();
            setImmediate(decrementSumOfAll, 'proposal', doc.region, doc.city, doc.type, doc.subType);
        }
    })
}

const proposalStatementEdit = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).end()
    } else {
        const body = req.body
        Proposal.findOne({}, (err, proposal) => {
            if (err || !proposal) {
                console.log(err || 'doc not found');
                res.status(500).end()
            } else {
                process.nextTick(
                    decrementSumOfAll, 
                    'proposal', 
                    proposal.region,
                    proposal.city,
                    proposal.type,
                    proposal.subType );

                proposal.region = body.region;
                proposal.city = body.city;
                proposal.type = body.type;
                proposal.subType = body.subType;
                proposal.unit = body.unit;
                proposal.price = body.price;
                proposal.available_quantity = body.available_quantity;
                proposal.is_plan_to_have = body.is_plan_to_have;
                proposal.oneTime = body.oneTime;
                proposal.continuous = body.continuous;
                proposal.description_one_word = body.description_one_word;
                proposal.comment = body.comment; 
                proposal.username = body.username;
                proposal.email = body.email;
                proposal.phone = body.phone;
                proposal.user_id = body.user_id;  
                if (body.is_plan_to_have) {
                    proposal.plan_to_have_quantity = body.plan_to_have_quantity
                    proposal.plan_to_have_price = body.plan_to_have_price
                    if (body.continuous) {
                        proposal.continuousType = body.continuousType
                        proposal.frequencyNum = body.frequencyNum
                    } else {
                        proposal.year = body.year
                        proposal.month = body.month
                        if (body.day !== '') {
                            proposal.day = body.day
                        }
                    }
                }  
                proposal.save((err, doc) => {
                    if (err || !doc) {
                        console.log(err || 'doc not found');
                        res.status(500).end()
                    } else {
                        res.send(doc);
                        setImmediate(
                            incrementSumOfAll,
                            'proposal',
                            doc.region,
                            doc.city,
                            doc.type,
                            doc.subType
                        )
                    }
                })        
            }
        })
    }
}

router.use('/*', checkAuth);

router.post('/demandStatement', demandValidator, demandStatementAdd);
router.put('/demandStatement', demandValidator, demandStatementEdit);
router.delete('/demandStatement', demandStatementDelete);

router.post('/proposalStatement', proposalValidator, proposalStatementAdd);
router.put('/proposalStatement', proposalValidator, proposalStatementEdit);
router.delete('/proposalStatement', proposalStatementDelete);

router.put('/updatePassword', updatePassvordValidator, updatePassword);

router.get('/getDemandsById', getDemandsById);

router.get('/getProposalsById', getProposalsById);

router.put('/changePersonalInfo', changePersonalInfoValidator, changePersonalInfo);

module.exports = router