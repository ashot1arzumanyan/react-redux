const langs = {
    en: require('../content/en.json'),
    ru: require('../content/ru.json'),
    hy: require('../content/hy.json')
}

module.exports = (req, res) => {
    const lang = req.params.id;
    let content;
    if (lang === 'en' || lang === 'ru') {
        content = langs[lang];
    } else {
        content = langs.hy;
    }
    // res.setHeader('Cache-Control', 'public, max-age=86400')
    res.setHeader('content-language', 'en, ru, hy');
    res.send(content)
};