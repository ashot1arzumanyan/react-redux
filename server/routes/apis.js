const { validationResult } = require('express-validator/check');
const bcrypt = require('bcryptjs');
const JWT = require('jsonwebtoken');
const cryptoRandomString = require('crypto-random-string');

const sgMail = require('../helpers/sgMail')
const { generateRegistrationConfirmationContent } = require('../helpers/generateContentOfMail');
const { generateResetPasswordConfirmationContent } = require('../helpers/generateContentOfMail');
const TempUser = require('../collections/TempUser');
const TempEmail = require('../collections/TempEmail');
const User = require('../collections/User');
const generateUserData = require('../helpers/generateUserData'); 

module.exports.login = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).send({ errors: errors.array() })
    } else {
        const { email, password } = req.body;
        User.findOne({ email: email}, (err, user) => {
            if(err || !user) {
                res.status(400).send({ errors: ['bad_email_or_password'] });
            } else {
                bcrypt.compare(password, user.password, (err, isMatch) => {
                    if(err || !isMatch) {
                        res.status(400).send({ errors: ['bad_email_or_password'] });
                    } else {
                        const data = generateUserData(user);
                        JWT.sign(
                            data,
                            process.env.MY_SECRET_KEY, 
                            { expiresIn: '1h' }, 
                            (err, token) => {
                                if(err) {
                                    console.error(err);
                                    req.status(500).end()
                                } else {
                                    res.send({ 
                                        access_token: token, 
                                        user: data 
                                    })
                                }
                            })
                    }
                })
            }
        })        
    }
}

module.exports.register = (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).send({ errors: errors.array() })
    } else {
        const { username, email, password, lang } = req.body;
        const createLength20_30 = Math.floor(Math.random()*10 + 20);
        const random_string = cryptoRandomString(createLength20_30);
        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                console.log(err);
                return res.status(500).send({ errors: ['something_wrong'] })
            }
            bcrypt.hash(password, salt, (err, hash) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ errors: ['something_wrong'] })
                }
                let tempUser = new TempUser({
                    username: username,
                    email: email,
                    password: hash,
                    random_string: random_string
                });
                tempUser.save((err, doc) => {
                    if(err) {
                        console.log(err);
                        res.status(500).send({ errors: ['something_wrong'] })
                    } else {
                        const content = generateRegistrationConfirmationContent(lang);
                        const msg = {
                            to: email,
                            from: 'mail@gyuxmterq.am',
                            subject: content.subject,
                            html: `<table>
                                        <tr>
                                            <td>
                                                ${content.hello},
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ${content.confirm}
                                            </td>
                                            <td>
                                                <a href='https://www.kanach-karmir.info/verifyEmail?key=${random_string}' >${content.link}</a>
                                            </td>
                                        </tr>
                                    </table> `
                        };
                        sgMail.send(msg)
                            .then(() => {
                                res.send({ user: {username: doc.username, email: doc.email}});
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(500).send({ errors: ['something_wrong'] })
                            })
                    }
                })
            })
        })
    }
}

module.exports.checkToken = (req, res) => {
    const authorizationHeaderSplited = 
    req.header('Authorization') ? 
        req.header('Authorization').split(' ') : '';
    if (authorizationHeaderSplited.length > 1) {
        JWT.verify(authorizationHeaderSplited[1], process.env.MY_SECRET_KEY, (err, decoded) => {
            if(err || !decoded) {
                console.log(err || 'not a decoded');
                res.status(403).end();
            } else {
                const data = generateUserData(decoded);
                res.send({
                    user: data 
                });
            }
        })
    } else {
        console.error(req.header('Authorization'));
        res.status(403).end();
    }
}

module.exports.resetPassword = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).send({ errors: errors.array() })
    } else {
        const { email, key } = req.body;
        TempEmail.findOne({email: email, random_string: key}, (err, doc) => {
            if (err || !doc) {
                console.log(err || 'not found in TempEmail');
                res.status(400).end()
            } else {
                bcrypt.genSalt(10, (err, salt) => {
                    if (err) {
                        console.log(err);
                        res.status(500).end()
                    } else {
                        bcrypt.hash(req.body.password, salt, (err, hash) => {
                            if (err) {
                                console.log(err);
                                res.status(500).end()
                            } else {
                                User.findOneAndUpdate({email: email}, {password: hash}, (err, user) => {
                                    if (err || !user) {
                                        console.log(err || 'user not found');
                                        res.status(500).end()
                                    } else {
                                        const data = generateUserData(user);
                                        JWT.sign(
                                            data,
                                            process.env.MY_SECRET_KEY, 
                                            { expiresIn: '1h' }, 
                                            (err, token) => {
                                                if(err) {
                                                    console.error(err);
                                                    res.status(500).end()
                                                } else {
                                                    res.send({ 
                                                        access_token: token, 
                                                        user: data 
                                                    })
                                                    TempEmail.findOneAndRemove({email: email, random_string: key}, (err) => {
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            console.log('TempEmail deleted');
                                                        }
                                                    })
                                                }
                                            })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
}

module.exports.resetPasswordSendEmail = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors.array());
        res.status(400).send({ errors: errors.array() })
    } else {
        const email = req.body.email;      
        User.findOne({email: email}, (err, doc) => {
            if (err || !doc) {
                console.log(err || 'user not found');
                res.status(400).send({errors: ['email_not_found']})
            } else {
                const createLength20_30 = Math.floor(Math.random()*10 + 20);
                const random_string = cryptoRandomString(createLength20_30);
                const tempEmail = new TempEmail({
                    email: email,
                    random_string: random_string
                })
                tempEmail.save((err) => {
                    if (err) {
                        console.log(err);
                        res.status(500).send({ errors: ['something_wrong'] })
                    } else {
                        const content = generateResetPasswordConfirmationContent(req.body.lang)
                        const msg = {
                            to: email,
                            from: 'mail@gyuxmterq.am',
                            subject: content.subject,
                            html: `<table>
                                        <tr>
                                            <td>
                                                ${content.hello},
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ${content.confirm}
                                            </td>
                                            <td>
                                                <a href='https://www.kanach-karmir.info/resetPassword?key=${random_string}&email=${email}' >${content.link}</a>
                                            </td>
                                        </tr>
                                    </table> `
                        };
                        sgMail.send(msg)
                            .then(() => {
                                res.end();
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(500).send({ errors: ['something_wrong'] })
                            })
                    }
                })
            }
        })
    }
}

const deleteTempUser = (email) => {
    TempUser.findOneAndDelete({email: email}, (err, res) => {
        if (err) {
            console.log('deleting tempUser   :  ' + err);
        }
        console.log('deleting tempUser   :  ' + res);
    })
}

module.exports.verifyEmail = (req, res) => {
    const key = req.query.key;
    TempUser.findOne({random_string: key}, (err, doc) => {
        if (err || !doc) {
            console.log(err || 'tempUser not found');
            res.status(500).send({ errors: ['something_wrong'] })
        } else {
            let user = new User({
                username: doc.username,
                email: doc.email,
                password: doc.password,
                date: doc.date
            });
            user.save((err, savedUser) => {
                if(err) {
                    console.error(err);
                    res.status(500).send({ errors: ['something_wrong'] })
                } else {
                    const data = generateUserData(savedUser);
                    JWT.sign(
                        data,
                        process.env.MY_SECRET_KEY, 
                        { expiresIn: '1h' }, 
                        (err, token) => {
                            if(err) {
                                console.error(err);
                                res.status(500).send({ errors: ['something_wrong'] })
                            } else {
                                res.send({ 
                                    access_token: token, 
                                    user: data 
                                })
                                setImmediate(deleteTempUser, doc.email);
                            }
                        })
                }
            })
        }
    })
}