const express = require('express');

const router = express.Router();
const myCache = require('../helpers/myCache');

const Proposal = require('../collections/Proposal');

router.get('/:linkToCachedAll', (req, res) => {
    let skip = Number(req.query.skip)
    console.log(skip);
    if (skip === 0) {
        Proposal.estimatedDocumentCount((err, count) => {
            if (err) {
                res.json({ ok: false })
            } else {
                Proposal.find().sort({ date: -1 }).skip(skip).limit(3).exec((err, proposals) => {
                    if (err) {
                        console.error(err);
                        res.json({ok: false})
                    } else {
                        const generateLink = Date.now();
                        setImmediate(generateCache, skip + 3, generateLink);
                        res.json({
                            ok: true, 
                            proposals: proposals,
                            skip: skip + 3,
                            count: count,
                            linkToCachedAll: generateLink
                        })
                    }
                })       
            }
        })
    } else {
        const linkToCachedAll = req.params.linkToCachedAll;
        myCache.get(linkToCachedAll, (err, data) => {
            if (err || !data) {
                console.log(err);
                Proposal.find({}).sort({date: -1}).skip(skip).limit(3).exec((err, proposals) => {
                    if (err) {
                        console.error(err);
                        return res.json({ok: false})
                    } else {
                        const generateLink = Date.now();
                        setImmediate(deleteCache, linkToCachedAll)
                        setImmediate(generateCache, skip + 3, generateLink);
                        return res.json({ok: true, proposals: proposals, skip: skip + 3})
                    }
                })
            } else {
                setImmediate(deleteCache, linkToCachedAll)
                res.json({ok: true, proposals: data})
            }
        })
    }

})

function generateCache(skip, linkToCachedAll) {
    console.log(skip, linkToCachedAll);
    Proposal.find({}).sort({date: -1}).skip(skip).exec((err, proposals) => {
        if (err) {
            console.error(err);
        } else {
            myCache.set(linkToCachedAll, proposals, 600, (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log(data);
                }
            })
        }
    })
}

function deleteCache(linkToCachedAll) {
    myCache.del(linkToCachedAll, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
        }
    })
}

module.exports = router