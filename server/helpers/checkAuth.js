const JWT = require('jsonwebtoken');

const checkAuth = (req, res, next) => {
    const authorizationHeader = req.header('Authorization');
    if (authorizationHeader && authorizationHeader.split(' ').length) {
        const access_tokenArray = req.header('Authorization').split(' ');
        JWT.verify(access_tokenArray[1], process.env.MY_SECRET_KEY, (err, decoded) => {
            if(err || !decoded) {
                console.log(err);
                return res.status(403).end()
            } else {
                req.user = decoded
                next()
            }
        })
    } else {
        return res.status(403).end()
    }
}

module.exports = checkAuth