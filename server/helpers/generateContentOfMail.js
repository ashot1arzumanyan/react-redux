module.exports.generateRegistrationConfirmationContent = (lang) => {
    let content = {};
    if (lang === 'ru') {
        content.subject = 'Подтверждение регистрации';
        content.hello = 'Здравствуйте';
        content.confirm = 'чтобы подтвердить регистрацию на сайте <b>kanach-karmir.info</b>, нажмите на эту ссылку:';
        content.link = 'подтвердить';
    } else if (lang === 'en') {
        content.subject = 'Registration confirmation';
        content.hello = 'Hello';
        content.confirm = 'to confirm the registration on <b>kanach-karmir.info</b>, click on this link:';
        content.link = 'confirm';
    } else {
        content.subject = 'Գրանցման հաստատում';
        content.hello = 'Բարև';
        content.confirm = 'հաստատելու համար գրանցումը <b>kanach-karmir.info</b> կայքում, սեղմեք այս հղմանը։';
        content.link = 'հաստատել';
    } 
    return content
}

module.exports.generateResetPasswordConfirmationContent = (lang) => {
    let content = {};
    if (lang === 'ru') {
        content.subject = 'Подтверждение смены пароля';
        content.hello = 'Здравствуйте';
        content.confirm = 'чтобы подтвердить смену пароля на сайте <b>kanach-karmir.info</b>, нажмите на эту ссылку:';
        content.link = 'подтвердить';
    } else if (lang === 'en') {
        content.subject = 'Password change confirmation';
        content.hello = 'Hello';
        content.confirm = 'to confirm the password change on <b>kanach-karmir.info</b>, click on this link:';
        content.link = 'confirm';
    } else {
        content.subject = 'Գաղտնաբառի փոփոխության հաստատում';
        content.hello = 'Բարև';
        content.confirm = 'Գաղտնաբառի փոփոխությունը <b>kanach-karmir.info</b> հաստատելու համար սեղմեք այս հղմանը։';
        content.link = 'հաստատել';
    } 
    return content
}