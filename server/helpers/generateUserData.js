const generateUserData = (user) => {
    const data = {
        username: user.username, 
        email: user.email, 
        _id: user._id,
    }
    if (user.phone) {
        data.phone = user.phone
    }
    return data;
}

module.exports = generateUserData;