const SumOfRegion = require('../collections/SumOfRegion');
const SumOfCity = require('../collections/SumOfCity');
const SumOfType = require('../collections/SumOfType');
const SumOfSubType = require('../collections/SumOfSubType');

const decrementSumOfAll = (proposalOrDemand, region, city, type, subType) => {
    const keyOfRegion = region + '.' + proposalOrDemand;
    const count = 'countOf_' + proposalOrDemand;
    const keyOfcity = city + '.' + proposalOrDemand;
    const keyOfType = type + '.' + proposalOrDemand;
    const keyOfSubType = subType + '.' + proposalOrDemand;
    SumOfRegion.findByIdAndUpdate(
        "5ba5fc0cb0a09e10a78fbdb1", 
        { 
            $inc: { [keyOfRegion]: -1, __v: -1 }
        },
        {
            new: true,
            setDefaultsOnInsert: true
        },
        (err) => {
            if(err) {
                console.error(err);
            } else {
                console.log(keyOfRegion + ' decremented by 1');
            }
        }
    )
    SumOfCity.findByIdAndUpdate(
        "5ba5fc0cb0a09e10a78fbdb2",
        { $inc: { [keyOfcity]: -1, __v: -1 } },
        {
            new: true,
            setDefaultsOnInsert: true
        },
        (err) => {
            if(err) {
                console.error(err);
            } else {
                console.log(keyOfcity + ' decremented by 1');
            }
        }
    )
    SumOfType.findByIdAndUpdate(
        "5ba5fc0cb0a09e10a78fbdb3",
        { $inc: { [keyOfType]: -1, __v: -1 } },
        {
            new: true,
            setDefaultsOnInsert: true
        },
        (err) => {
            if(err) {
                console.error(err);
            } else {
                console.log(keyOfType + ' decremented by 1');
            }
        }
    )
    SumOfSubType.findByIdAndUpdate(
        "5ba5fc0cb0a09e10a78fbdb4",
        { $inc: { [keyOfSubType]: -1, __v: -1 } },
        {
            new: true,
            setDefaultsOnInsert: true
        },
        (err) => {
            if(err) {
                console.error(err);
            } else {
                console.log(keyOfSubType + ' decremented by 1');
            }
        }
    )
}

module.exports = decrementSumOfAll;