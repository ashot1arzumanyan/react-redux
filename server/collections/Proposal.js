const mongoose = require('mongoose');

const options = { versionKey: false };

const ProposalSchema = new mongoose.Schema({
    region: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    subType: {
        type: String,
        required: true
    },
    unit: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    available_quantity: {
        type: Number,
        required: true
    },
    description_one_word: {
        type: String,
        maxlength: 25
    },
    comment: {
        type: String,
    },
    is_plan_to_have: {
        type: Boolean,
        required: true
    },
    oneTime: {
        type: Boolean,
        required: true 
    },
    continuous: {
        type: Boolean,
        required: true
    },
    plan_to_have_quantity: {
        type: Number,
        required: function() {
            return this.is_plan_to_have
        }
    },
    plan_to_have_price: {
        type: Number,
        required: function() {
            return this.is_plan_to_have
        }
    },
    year: {
        type: String,
        required: function() {
            return this.is_plan_to_have && this.oneTime
        }
    },
    month: {
        type: String,
        required: function() {
            return  this.is_plan_to_have && this.oneTime
        }
    },
    day: {
        type: String
    },
    continuousType: {
        type: String,
        required: function() {
            return this.continuous
        }
    },
    frequencyNum: {
        type: String,
        required: function() {
            return this.continuous
        }
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    phone: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now 
    },
}, options);

const Proposal = mongoose.model('Proposal', ProposalSchema);

module.exports = Proposal;