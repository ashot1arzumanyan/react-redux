const mongoose = require('mongoose');

const SumOfRegionSchema = new mongoose.Schema({
  erevan: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  aragacotn: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  ararat: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  armavir: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  gexarkunik: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  kotayk: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  lori: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  shirak: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  syunik: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  tavush: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
  vayoc_cor: {
    proposal: {
      type: Number,
      default: 0
    },
    demand: {
      type: Number,
      default: 0
    }
  },
});

const SumOfRegion = mongoose.model('SumOfRegion', SumOfRegionSchema); 

module.exports = SumOfRegion;