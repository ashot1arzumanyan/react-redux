const mongoose = require('mongoose');
   
const SumOfCitySchema = new mongoose.Schema({
    erevan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ashtarak: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    aparan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    talin: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ararat: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    artashat: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    masis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    vedi: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    armavir: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    vaxarshapat: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mecamor: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    gavar: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    jambarak: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    martuni: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    sevan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    vardenis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    abovyan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    byurexavan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    exvard: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    nor_hajn: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    charencavan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    alaverdi: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    axtala: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    tumanyan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    shamlux: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    spitak: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    stepanavan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    tashir: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    artik: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    gyumri: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    maralik: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    agarak: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    goris: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    dastakert: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kapan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mexri: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    sisian: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    qajaran: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ayrum: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    berd: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    dilijan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ijevan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    noyemberyan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    exegnacor: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    jermuk: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    vayq: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    }
});

const SumOfCity = mongoose.model('SumOfCity', SumOfCitySchema);

module.exports = SumOfCity;