const mongoose = require('mongoose');
   
const SumOfTypeSchema = new mongoose.Schema({
    katnamterq: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    msamterq: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kendaniner: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    trchunner_cu: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ckner_ikra: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mexvamterq: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    banjarexen: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mirg: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    hataptux: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    grain_culture: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ynkuzexen: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    chir_ev_charaz: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    pahaconer: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kanachexen: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    busakan_teyer: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    tnkiner: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    }
});

const SumOfType = mongoose.model('SumOfType', SumOfTypeSchema);

module.exports = SumOfType;