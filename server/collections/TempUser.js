const mongoose = require('mongoose');

const options = { versionKey: false };

const TempUserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        min: 3,
        max: 33,
    },
    email: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    random_string: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now,
        expires: '30m'
    }
}, options);

const TempUser = mongoose.model('TempUser', TempUserSchema);

module.exports = TempUser;