const mongoose = require('mongoose');

const options = { versionKey: false };

const DemandSchema = new mongoose.Schema({
    region: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    subType: {
        type: String,
        required: true
    },
    unit: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quantity: {
        type: String,
    },
    oneTime: {
        type: Boolean,
        required: true 
    },
    continuous: {
        type: Boolean,
        required: true
    },
    is_mark_the_date: {
        type: Boolean,
        required: true
    },
    year: {
        type: String,
        required: function() {
            return this.is_mark_the_date
        }
    },
    month: {
        type: String,
        required: function() {
            return this.is_mark_the_date
        }
    },
    day: {
        type: String
    },
    continuousType: {
        type: String,
        required: function() {
            return this.continuous
        }
    },
    frequencyNum: {
        type: String,
        required: function() {
            return this.continuous
        }
    },
    description_one_word: {
        type: String,
        maxlength: 25
    },
    comment: {
        type: String,
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    phone: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now 
    },

}, options);

const Demand = mongoose.model('Demand', DemandSchema);

module.exports = Demand;