const mongoose = require('mongoose')

const options = { versionKey: false };

const TempEmailSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    random_string: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now,
        expires: '30m'
    }
}, options)

module.exports = mongoose.model('TempEmail', TempEmailSchema)