const mongoose = require('mongoose');
   
const SumOfSubTypeSchema = new mongoose.Schema({
    panir: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kat: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    macun: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    katnashor: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    karag: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    yux: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    garan_mis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    khozi_mis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    tavari_mis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    jagari_mis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    havi_mis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    aqlori_mis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    jagar: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kov: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    hort: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    vochxar: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    gar: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    khoz: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    khochkor: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    cu: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ikra: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mexr: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mexvamom: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    propolis: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mexvayntaniq: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mexvamayr: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    petak: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    caxkaposhi: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    varung: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    pomidor: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kartofil: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kaxamb: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    sxtor: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    badrijan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    khncor: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    tanc: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    ciran: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    nur: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    tuz: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    dzmeruk: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    limon: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    khaxox: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mori: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    malina: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    haxarj: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    aloj: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mosh: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    hon: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    coren: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    gari: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    kukuruz: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    grechka: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    hajar: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    chamich: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    cirani_chir: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    salori_chir: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    chor_tut: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    cirani_jem: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    khncor_jem: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    hamem: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    petrushka: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    rehan: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    aveluk: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    nana: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    urc: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    mrgatu_careri: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    hataptxayin_tperi: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    },
    banjarexenayin_buyseri: {
        proposal: {
            type: Number,
            default: 0
        },
        demand: {
            type: Number,
            default: 0
        }
    }
});

const SumOfSubType = mongoose.model('SumOfSubType', SumOfSubTypeSchema);

module.exports = SumOfSubType;