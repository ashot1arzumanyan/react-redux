const mongoose = require('mongoose');

const options = { versionKey: false };

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        min: 3,
        max: 33,
    },
    email: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    }
}, options);

const User = mongoose.model('User', UserSchema);

module.exports = User;