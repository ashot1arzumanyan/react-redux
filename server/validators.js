const { check, oneOf } = require('express-validator/check');

const regions = require('../client/src/constants/regions');
const cities = require('../client/src/constants/cities');
const types = require('../client/src/constants/types');
const subTypes = require('../client/src/constants/subTypes');
const units = require('../client/src/constants/units');

module.exports.registerValidator = [
    check('email').isEmail().withMessage('email_not_correct'),
    check('username').isLength({ min: 3, max: 33 }).withMessage('username_not_correct'),
    check('password').isLength({ min: 7, max: 77 }).withMessage('password_not_correct'),
    check('repeat_password')
        .custom((value, { req }) => value === req.body.password).withMessage('passwords_not_matched')
];

module.exports.loginValidator = [
    check('email').isEmail().withMessage('email_not_correct'),
    // check('password').isLength({ min: 7, max: 77 }).withMessage('bad_email_or_password')
];

module.exports.changePersonalInfoValidator = [
    check('username').isLength({ min: 3, max: 33 }).withMessage('username_not_correct'),
];

module.exports.updatePassvordValidator = [
    check('old_password').isLength({min: 7, max: 77}).withMessage('wrong_current_password'),
    check('password').isLength({min: 7, max: 77}).withMessage('password_not_correct'),
    check('repeat_password')
        .custom((value, { req }) => value === req.body.password).withMessage('passwords_not_matched')
];

module.exports.resetPasswordValidator = [
    check('email').isEmail().withMessage('something_wrong'),
    check('key').isLength({ min: 19, max: 31 }).withMessage('something_wrong'),
    check('password').isLength({ min: 7, max: 77 }).withMessage('password_not_correct'),
    check('repeat_password')
        .custom((value, { req }) => value === req.body.password).withMessage('passwords_not_matched')   
];

module.exports.resetPasswordSendEmailValidator = [
    check('email').isEmail().withMessage('email_not_correct')
];

module.exports.demandValidator = [
    check('region').isIn(regions),
    check('city').isIn(cities),
    check('type').isIn(types),
    check('subType').isIn(subTypes),
    check('unit').isIn(units),
    check('price').not().isEmpty(),
    check('quantity').not().isEmpty(),
    check('oneTime').isBoolean(),
    check('continuous').isBoolean(),
    check('continuous').custom((value, { req }) => {
        if (value) {
            const theseShuldBe = req.body.continuousType && req.body.frequencyNum ? true : false;
            if (!theseShuldBe) {
                throw new Error('...');
            }
        }
        return true 
    }),
    check('is_mark_the_date').isBoolean().custom((value, { req }) => {
        if (value) {
            const theseShuldBe = req.body.year && req.body.month ? true : false;
            if (!theseShuldBe) {
                throw new Error('...');
            }
        }
        return true
    }),
    check('description_one_word').isLength({max: 25})
        .custom(value => { 
            if (value) {
                if (value.split(' ').length > 1) {
                    throw new Error('...')
                }
            }
            return true
        }),
    check('comment').isLength({max: 1000}),
    check('username').isLength({min: 3}),
    oneOf([
        check('email').isEmail(),
        check('phone').not().isEmpty()
    ])
];

module.exports.proposalValidator = [
    check('region').isIn(regions),
    check('city').isIn(cities),
    check('type').isIn(types),
    check('subType').isIn(subTypes),
    check('unit').isIn(units),
    check('price').not().isEmpty(),
    check('available_quantity').not().isEmpty(),
    check('description_one_word').isLength({max: 25})
        .custom(value => { 
            if (value) {
                if (value.split(' ').length > 1) {
                    throw new Error('...')
                }
            }
            return true
        }),
    check('comment').isLength({max: 1000}),
    check('username').isLength({min: 3}),
    oneOf([
        check('email').isEmail(),
        check('phone').not().isEmpty()
    ]),
    check('oneTime').isBoolean(),
    check('continuous').isBoolean(),
    check('is_plan_to_have').isBoolean().custom((value, { req }) => {
        if (value) {
            if (!(req.body.plan_to_have_quantity && req.body.plan_to_have_price)) {
                return false
            }
            if (req.body.continuous) {
                return req.body.continuousType && req.body.frequencyNum ? true : false;
            } else {
                return req.body.year && req.body.month ? true : false
            }
        } else {
            return true
        }
    })
]