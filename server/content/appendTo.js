const fs = require('fs')
const path = require('path')

const from = 'withEmptyValues.json';
const to = 'ru.json';

fs.readdir(__dirname, (err, files) => {
    if (err) throw err;
    console.log(files);
    const newPath = 'copy-' + new Date().toString();
    fs.mkdir(path.join(__dirname, newPath));
    files.filter(file => file.includes('.json')).forEach(file => {
        fs.copyFile(path.join(__dirname, file), path.join(__dirname, newPath, file), (err => {
            if (err) console.error(err);
        }))
    })
})

let hy;
let en;

fs.readFile(path.join(__dirname, from), (err, data) => {
    if (err) console.error(err);
    hy = JSON.parse(data);
    fs.readFile(path.join(__dirname, to), (err, data) => {
        if (err) console.error(err);
        en = JSON.parse(data);

        compareAndAdd(Object.keys(hy));
        Object.keys(hy).forEach(key => {
            compareAndAddNested(Object.keys(hy[key]), key)
        })
        Object.keys(hy).forEach(key => {
            if (Object.keys(hy[key]).length) {
                Object.keys(hy[key]).forEach(nestedKey => {
                    if ( typeof hy[key][nestedKey] !== 'string') {
                        compareAndAddNestedNested(Object.keys(hy[key][nestedKey]), key, nestedKey)
                    }
                })
            }
        });
        fs.writeFile(path.join(__dirname, to), JSON.stringify(en), (err => {
            if (err) console.error(err);
        }))
    })
})


function compareAndAdd(array) {
    array.forEach(key => {
        if (typeof en[key] === 'undefined') {
            en[key] = ""
        }
    })
}

function compareAndAddNested(array, nestedKey) {
    array.forEach(key => {
        if (typeof en[nestedKey][key] === 'undefined') {
            if ( typeof en[nestedKey] !== 'object' ) {
                en[nestedKey] = {}
            }
            en[nestedKey][key] = "";
        }
    })
}

function compareAndAddNestedNested(array, nestedKey, nestedNestedKey) {
    array.forEach(key => {
        if (typeof en[nestedKey][nestedNestedKey][key] === 'undefined') {
            if ( typeof en[nestedKey][nestedNestedKey] !== 'object' ) {
                en[nestedKey][nestedNestedKey] = {}
            }
            en[nestedKey][nestedNestedKey][key] = ""
        }
    })
    // console.log(en);
}